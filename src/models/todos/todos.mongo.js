const mongoose = require('mongoose')

const todosSchema = new mongoose.Schema({
    taskId: Number,
    description: {
        type: String,
        required: true
    },
    isDone: {
        type: Boolean,
        default: false,
        required: true
    } 
})

module.exports = mongoose.model('ToDo', todosSchema)