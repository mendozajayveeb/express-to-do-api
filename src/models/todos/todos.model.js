const ToDo = require('./todos.mongo')
const Error = require('../../helpers/Error')

const ToDosArray = [
    {
        id: 1,
        name: 'Clean dishes',
        status: false,
    },
    {
        id: 2,
        name: 'Get to work',
        status: false,
    },
    {
        id: 3,
        name: 'Read books',
        status: false,
    },
]

const defaultTaskId = 0

const getLatestTaskId = async () => {
    try {
        const latestTask = await ToDo.findOne({}).sort('-taskId')

        if (!latestTask) {
            return defaultTaskId
        }

        return latestTask.taskId
    } 
    catch (error) {
        return Error.raise('INTERNAL_SERVER_ERROR')
    }
}

const findTodo = async (filter) => {
    try {
        return await ToDo.findOne(filter, { _id: 0, __v: 0 })
    } 
    catch (error) {
        return Error.raise('INTERNAL_SERVER_ERROR')
    }
}

const getAllToDos = async (skip, limit) => {
    try {
        return await ToDo
            .find(
                { isDone: false }, 
                { _id: 0, __v: 0 }
            )
            .skip(skip)
            .limit(limit)
            // .sort({ taskId: 1 })
    } 
    catch (error) {
        return Error.raise('INTERNAL_SERVER_ERROR')
    }
    
}

const getOneToDo = async (id) => {
    try {
        return await findTodo({ taskId: id})
    } 
    catch (error) {
        return Error.raise('INTERNAL_SERVER_ERROR')
    }
}

const addNewToDo = async (newToDo) => {
    try {
        const taskId = await getLatestTaskId() + 1

        newToDo.taskId = taskId
        const newTask = new ToDo(newToDo)

        return await newTask.save()
    } 
    catch (error) {
        throw new Error(error)
    }
}

const updateOneToDo = async (id, payload) => {
    try {
        await ToDo.findOneAndUpdate({ taskId: id }, { ...payload })
        
        return await getOneToDo(id)
    } 
    catch (error) {
        throw new Error(error)
    }
    
}

module.exports = {
    getAllToDos,
    findTodo,
    getOneToDo,
    addNewToDo,
    updateOneToDo
}