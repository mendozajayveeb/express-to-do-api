const express = require('express')

const toDoRouter = require('./todos/todos.router')

const Router = express.Router()

Router.use('/todos', toDoRouter)

module.exports = Router