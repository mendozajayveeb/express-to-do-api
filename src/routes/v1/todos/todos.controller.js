const {
    getOneToDo,
    getAllToDos,
    addNewToDo,
    updateOneToDo
} = require('../../../models/todos/todos.model')

const Error = require('../../../helpers/Error')
const { getPagination } = require('../../../helpers/Utility')

const e = {
    NOT_FOUND: 'NOT_FOUND',
    INVALID_REQUEST: 'INVALID_REQUEST',
    INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
    UNAUTHORIZED_ACCESS: 'UNAUTHORIZED_ACCESS'
}

const httpGetAllToDos = async (req, res) => {
    const { skip, limit } = getPagination(req.query)
    let response = await getAllToDos(skip, limit)

    if (response.error) {
        return res.error(response)
    }

    return res.ok(response)
}

const httpGetOneToDo = async (req, res) => {
    const id = req.params.id

    let response = await getOneToDo(id)

    if (!response) {
        return res.error(Error.raise(e.NOT_FOUND))
    }

    if (response.error) {
        return res.error(response)
    }

    return res.ok(response)
}

const httpAddNewToDo = async (req, res) => {
    const body = req.body
    
    if (!body.description) {
        return res.error(Error.raise(e.INVALID_REQUEST))
    }

    const response = await addNewToDo(body)

    if (response.error) {
        return res.error(response)
    }

    return res.ok({
        taskId: response.taskId,
        description: response.description,
        isDone: response.isDone,
    })
}

const httpUpdateOneToDo = async (req, res) => {
    const id = req.params.id
    const description = req.body.description

    if (!description) {
        return res.error(Error.raise(e.INVALID_REQUEST))
    }

    if (!await getOneToDo(id)) {
        return res.error(Error.raise(e.NOT_FOUND))
    }

    const response = await updateOneToDo(id, { description })

    if (response.error) {
        return res.error(response)
    }
    
    return res.ok(response)
}

const httpRemoveOneToDo = async (req, res) => {
    const id = req.params.id

    if (!await getOneToDo(id)) {
        return res.error(Error.raise(e.NOT_FOUND))
    }

    const response = await updateOneToDo(id, { isDone: true })

    if (response.error) {
        return res.error(response)
    }

    return res.ok(response)
}

module.exports = {
    httpGetAllToDos,
    httpGetOneToDo,
    httpAddNewToDo,
    httpUpdateOneToDo,
    httpRemoveOneToDo,
}