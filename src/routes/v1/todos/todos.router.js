const express = require('express')

const {
    httpGetAllToDos,
    httpGetOneToDo,
    httpAddNewToDo,
    httpUpdateOneToDo,
    httpRemoveOneToDo,
} = require('./todos.controller')

const Router = express.Router()

Router.get('/', httpGetAllToDos)
Router.get('/:id', httpGetOneToDo)
Router.post('/', httpAddNewToDo)
Router.put('/:id', httpUpdateOneToDo)
Router.delete('/:id', httpRemoveOneToDo)

module.exports = Router
