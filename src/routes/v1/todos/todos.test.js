const request = require('supertest')

const app = require('../../../app')
const { mongoConnection, mongoDisconnect } = require('../../../db/mongo')

describe('Run To-Do API Tests', () => {
    beforeAll(async () => {
        await mongoConnection()
    })

    afterAll(async () => {
        await mongoDisconnect()
    })

    describe('Test GET /v1/todos', () => {
        test('It should respond with 200 success', async () => {
            await request(app)
                .get('/v1/todos')
                .expect('Content-Type', /json/)
                .expect(200)
        })
    })

    describe('Test POST /v1/todos', () => {
        let todoRequest = {
            description: 'Hire this dev',
        }

        test('It should respond with 200 created', async () => {
            const response = await request(app)
                .post('/v1/todos')
                .send(todoRequest)
                .expect('Content-Type', /json/)
                .expect(200)
        })

        test('It should catch missing required field', async () => {
            delete todoRequest.description

            const response = await request(app)
                .post('/v1/todos')
                .send(todoRequest)
                .expect('Content-Type', /json/)
                .expect(400)

            expect(response.body).toStrictEqual({
                error: {
                    message: 'An error occured.',
                    details: 'Missing/Invalid request.',
                },
            })
        })
    })

    describe('Test GET /v1/todos/{id}', () => {
        test('It should respond with 200 success', async () => {
            await request(app)
                .get('/v1/todos/1')
                .expect('Content-Type', /json/)
                .expect(200)
        })

        test('It should respond with 404 not found', async () => {
            const response = await request(app)
                .get('/v1/todos/98524547711111111')
                .expect('Content-Type', /json/)
                .expect(404)

            expect(response.body).toStrictEqual({
                error: {
                    message: 'An error occured.',
                    details: 'Record not found.',
                },
            })
        })
    })

    describe('Test PUT /v1/todos/{id}', () => {
        let todoUpdRequest = {
            description: 'Hire this dev right away',
        }

        test('It should respond with 200 success', async () => {
            await request(app)
                .put('/v1/todos/1')
                .send(todoUpdRequest)
                .expect('Content-Type', /json/)
                .expect(200)
        })

        test('It should catch missing required field', async () => {
            delete todoUpdRequest.description

            const response = await request(app)
                .post('/v1/todos')
                .send(todoUpdRequest)
                .expect('Content-Type', /json/)
                .expect(400)

            expect(response.body).toStrictEqual({
                error: {
                    message: 'An error occured.',
                    details: 'Missing/Invalid request.',
                },
            })
        })
    })

    describe('Test DELETE /v1/todos/{id}', () => {
        test('It should return 200 success', async () => {
            await request(app)
                .delete('/v1/todos/1')
                .expect('Content-Type', /json/)
                .expect(200)
        })

        test('It should return 404 not found', async () => {
            await request(app)
                .delete('/v1/todos/123232323232223323')
                .expect('Content-Type', /json/)
                .expect(404)
        })
    })
})
