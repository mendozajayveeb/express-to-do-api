const express = require('express')
const helmet = require('helmet')
var morgan = require('morgan')

const v1Routes = require('./routes/v1')

const app = express()

app.use(helmet())
app.use(express.json())
app.use(morgan('combined'))

app.use((req, res, next) => {
    res.error = (err) => {
        return res.status(err.status).json({
            error: {
                message: 'An error occured.',
                details: err.error.details
            }
        })
    }

    res.ok = (data) => {
        return res.status(200).json({
            message: 'Success',
            data
        })
    }

    next()
}) 

app.use('/v1', v1Routes)

app.use(function (req, res) {
    res.status(404).json({
        error: {
            message: 'An error occured.',
            details: 'Resource not found.'
        }
    })
})

module.exports = app