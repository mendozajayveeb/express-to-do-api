module.exports = {
    get: function (e) {
        const errorSet = {
            NOT_FOUND: {
                status: 404,
                error: {
                    details: 'Record not found.',
                },
            },

            INVALID_REQUEST: {
                status: 400,
                error: {
                    details: 'Missing/Invalid request.',
                },
            },

            INTERNAL_SERVER_ERROR: {
                status: 500,
                error: {
                    details: 'Internal server error.',
                },
            },

            UNAUTHORIZED_ACCESS: {
                status: 401,
                error: {
                    details: 'Unauthorized access.',
                },
            },
        }

        return errorSet[e]
    },

    raise: function (e) {
        return this.get(e)
    }
}