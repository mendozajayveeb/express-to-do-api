# Express - To Do API

### Getting started

To clone repository and test it locally, run the following command:

```
git clone https://gitlab.com/mendozajayveeb/express-to-do-api.git
cd express-to-do-api
cp .env.example .env
npm install
npm start
```

To run unit test, run the command:
```
npm test
```

### Postman Collection and Env variables

For a sample postman collection and environment variables, contact developer via email: mendozajayveeb@gmail.com