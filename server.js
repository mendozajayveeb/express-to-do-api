const http = require('http')

const mongoose = require('mongoose')
require('dotenv').config()

const app = require('./src/app')
const { mongoConnection } = require('./src/db/mongo')

const server = http.createServer(app)

const PORT = process.env.PORT || 8000

const startServer = async () => {
    await mongoConnection()
    
    server.listen(PORT, () => console.log(`Server running on port ${PORT}`))
}

startServer()